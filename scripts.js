var grid = ["E", "E", "E", "E", "E", "E", "E", "E", "E"];
var enabled = false
var playerChar = "";
var aiChar = "";


function switchAi(difficulty) {

	aiDifficulty = difficulty;

	if (difficulty == "easy") {
		document.getElementById("hardSwitch").classList.remove("activeSwitch");
		document.getElementById("easySwitch").classList.add("activeSwitch");

	} else {
		document.getElementById("easySwitch").classList.remove("activeSwitch");
		document.getElementById("hardSwitch").classList.add("activeSwitch");
	}
}


function newGame(inputChar) {

	setMessageBox("");
	setGameStateMessage("");

	playerChar = inputChar;

	if (playerChar == "X") {
		aiChar = "O";
		hardOpeningMove = false;

	} else {
		aiChar = "X";
		hardOpeningMove = true;
	}

	for (i = 0; i < grid.length; i++) {
		grid[i] = "E";

		setButtonContent(i, "E");
	}

	enabled = true;

	if (playerChar == "O") {
		aiPlay();
	}
}


function checkVictory(player) {

	//this must be the ugliest, longest and most unreadable line of code I have ever written :'D
	//could be made more elegant by only checking the 2 or 3 rows in which the recently played square can be, or at least splitting the conditional to multiple if-elses, but then again this is simpler even if it is ugly
	if (((grid[0] == player) && (grid[1] == player) && (grid[2] == player)) || ((grid[3] == player) && (grid[4] == player) && (grid[5] == player)) || ((grid[6] == player) && (grid[7] == player) && (grid[8] == player)) || ((grid[0] == player) && (grid[3] == player) && (grid[6] == player)) || ((grid[1] == player) && (grid[4] == player) && (grid[7] == player)) || ((grid[2] == player) && (grid[5] == player) && (grid[8] == player)) || ((grid[0] == player) && (grid[4] == player) && (grid[8] == player)) || ((grid[2] == player) && (grid[4] == player) && (grid[6] == player))) {
		return true;

	}

	return false;
}


function gridIsFull() {

	for (i = 0; i < grid.length; i++) {

		if (grid[i] == "E") {
			return false;
		}
	}

	return true;
}


// return false if the game is over
function checkGameState(player) {

	if (checkVictory(player)) {
		var message = player + " wins the game!"
		setGameStateMessage(message);

		enabled = false;
		return false;

	} else if (gridIsFull()) {
		setGameStateMessage("The game is a draw.");

		enabled = false;
		return false;

	} else {
		return true;
	}
}


function playSquare(square, player) {

	grid[square] = player;

	setButtonContent(square, player);
}


function activate(square) {

	setMessageBox("");

	if (enabled) {

		if (grid[square] == "E") {
			playSquare(square, playerChar);

			var continueGame = checkGameState(playerChar);
			if (continueGame) {
				aiPlay();
			}

		} else {
			setMessageBox("Square is not empty!");
		}

	} else {
		setMessageBox("Select X or O to start a new game.");
	}
}


function setButtonContent(id, content) {

	id = "button" + id.toString();

	if (content == "E") {
		document.getElementById(id).src = "images/empty.png"

	} else if (content =="X") {
		document.getElementById(id).src = "images/x.png"

	} else if (content == "O") {
		document.getElementById(id).src = "images/o.png"
	}

}


function setMessageBox(message) {
	document.getElementById("messageBox").innerHTML = message;
}


function setGameStateMessage(message) {
	document.getElementById("gameStateMessage").innerHTML = message;
}

