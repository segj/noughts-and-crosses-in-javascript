// oh boy oh no I feel like this file is made of noodles and other pasta

var aiDifficulty = "easy"
var hardOpeningMove = false;


function aiPlay() {

	if (aiDifficulty == "easy") {
		aiRandomPlay();

	} else {
		aiPerfectPlay();
	}

	hardOpeningMove = false;
}


function aiRandomPlay() {

	var legalMoves = ["0", "1", "2", "3", "4", "5", "6", "7", "8"];
	legalMoves = checkLegalMoves(legalMoves);

	playSquare(legalMoves[randomNumber(legalMoves.length - 1)], aiChar);
	checkGameState(aiChar);
}


function aiPerfectPlay() {

	var square = -1;

	if (hardOpeningMove) {
		square = moveOpening();

	} else {
		square = moveWin();
	}

	if (square == -1) {
		console.log("hard AI square never got changed from -1!");
	}
	
	console.log("at square", square);
	playSquare(square, aiChar);
	checkGameState(aiChar);
}


function moveOpening() {

	square = randomNumber(10);

	if ((square == 0) || (square == 2) || (square == 6) || (square == 8)) {
		return square;

	} else {
		return 4;
	}
}


function moveWin() {

	var legalMoves = findWinningMoves(aiChar);
	legalMoves = checkLegalMoves(legalMoves);

	if (legalMoves.length > 0) {
		console.log("AI played a win move");
		return legalMoves[randomNumber(legalMoves.length - 1)];

	} else {
		return moveBlock();
	}
}


function moveBlock() {

	var legalMoves = findWinningMoves(playerChar);
	legalMoves = checkLegalMoves(legalMoves);

	if (legalMoves.length > 0) {
		console.log("AI played a block move");
		return legalMoves[randomNumber(legalMoves.length - 1)];

	} else {
		return moveCenter();
	}
}


function moveCenter() {

	if (grid[4] == "E") {
		console.log("AI played a center move");
		return 4;

	} else {
		return moveOppositeCorner();
	}
}


function moveOppositeCorner() {

	var legalMoves = new Array();

	if (checkCornerOwnership(8)) {
		legalMoves.push(0);

	} if (checkCornerOwnership(6)) {
		legalMoves.push(2);

	} if (checkCornerOwnership(2)) {
		legalMoves.push(6);

	} if (checkCornerOwnership(0)) {
		legalMoves.push(8);
	}

	legalMoves = checkLegalMoves(legalMoves);

	if (legalMoves.length > 0) {
		console.log("AI played an opposite corner move");
		return legalMoves[randomNumber(legalMoves.length - 1)];

	} else {
		return moveRandomCorner();
	}
}


function moveRandomCorner() {

	var legalMoves = [0, 2, 6, 8];
	legalMoves = checkLegalMoves(legalMoves);

	if (legalMoves.length > 0) {
		console.log("AI played a random corner move");
		return legalMoves[randomNumber(legalMoves.length - 1)];

	} else {
		return moveSide();
	}
}


function moveSide() {

	var legalMoves = [1, 3, 5, 7];
	legalMoves = checkLegalMoves(legalMoves);

	if (legalMoves.length > 0) {
		console.log("AI played a random side move");
		return legalMoves[randomNumber(legalMoves.length - 1)];

	} else {
		return -1;
	}
}


function randomNumber(max) {

	return (Math.floor(Math.random() * (max + 1)));
}


function checkCornerOwnership(index) {

	if (grid[index] == playerChar) {
		return true;
	}

	return false;
}


function checkLegalMoves(moves) {

	for (i = moves.length; i >= 0; i--) {

		if (grid[moves[i]] != "E") {
			moves.splice(i, 1);
		}
	}

	return moves;
}


function findWinningMoves(ch) {
	// ch is short for the character we are looking for

	var moves = new Array();

	if (((grid[1] == ch) && (grid[7] == ch)) || ((grid[3] == ch) && (grid[5] == ch)) || ((grid[0] == ch) && (grid[8] == ch)) || ((grid[2] == ch) && (grid[6] == ch))) {
		moves.push(4);
	}

	if (((grid[3] == ch) && (grid[6] == ch)) || ((grid[1] == ch) && (grid[2] == ch)) || ((grid[4] == ch) && (grid[8] == ch))) {
		moves.push(0);
		
	} if (((grid[5] == ch) && (grid[8] == ch)) || ((grid[0] == ch) && (grid[1] == ch)) || ((grid[4] == ch) && (grid[6] == ch))) {
		moves.push(2);
		
	} if (((grid[0] == ch) && (grid[3] == ch)) || ((grid[7] == ch) && (grid[8] == ch)) || ((grid[4] == ch) && (grid[2] == ch))) {
		moves.push(6);
		
	} if (((grid[2] == ch) && (grid[5] == ch)) || ((grid[6] == ch) && (grid[7] == ch)) || ((grid[4] == ch) && (grid[0] == ch))) {
		moves.push(8);
	}

	if (((grid[4] == ch) && (grid[7] == ch)) || ((grid[0] == ch) && (grid[2] == ch))) {
		moves.push(1);

	} if (((grid[0] == ch) && (grid[6] == ch)) || ((grid[4] == ch) && (grid[5] == ch))) {
		moves.push(3);

	} if (((grid[2] == ch) && (grid[8] == ch)) || ((grid[3] == ch) && (grid[4] == ch))) {
		moves.push(5);

	} if (((grid[1] == ch) && (grid[4] == ch)) || ((grid[6] == ch) && (grid[8] == ch))) {
		moves.push(7);
	}

	return moves;
}

